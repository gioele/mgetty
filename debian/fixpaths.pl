#! /usr/bin/perl -w

# fix path/programmname for the list of files
# /usr/local/bin/perl -> perl
# /usr/lib/sendmail -> /usr/sbin/sendmail
# /bin/ksh -> /bin/bash
# /usr/local/bin/vm -> /usr/bin/vm

@fix=(
'debian/mgetty-docs/usr/share/doc/mgetty/contrib/faxmail-smail',
'debian/mgetty-docs/usr/share/doc/mgetty/frontends/faxmail/faxmail',
'debian/mgetty-docs/usr/share/doc/mgetty/contrib/watchit.pl',
'debian/mgetty-docs/usr/share/doc/mgetty/frontends/mail2fax/mail2fax.rc',
'debian/mgetty-docs/usr/share/doc/mgetty/frontends/mmdf-mail2fax/faxgate.pl',
'debian/mgetty-docs/usr/share/doc/mgetty/examples/fax',
'debian/mgetty-docs/usr/share/doc/mgetty/examples/new_fax.hpa',
'debian/mgetty-docs/usr/share/doc/mgetty/examples/new_fax.mime1',
'debian/mgetty-docs/usr/share/doc/mgetty/voice/examples/scripts/button.sh',
'debian/mgetty-docs/usr/share/doc/mgetty/voice/examples/scripts/demo.pl',
'debian/mgetty-docs/usr/share/doc/mgetty/voice/examples/scripts/demo.sh',
'debian/mgetty-docs/usr/share/doc/mgetty/voice/examples/scripts/dtmf.sh',
'debian/mgetty-docs/usr/share/doc/mgetty/voice/examples/scripts/events.sh',
'debian/mgetty-docs/usr/share/doc/mgetty/voice/examples/scripts/message.sh',
'debian/mgetty-docs/usr/share/doc/mgetty/voice/examples/scripts/vmtest.sh' );

foreach $i (@fix) {
	print `sed ' \
		s|/usr/local/bin/perl5|/usr/bin/perl|; \
		s|/usr/local/bin/perl|/usr/bin/perl|; \
		s|/usr/lib/sendmail|/usr/sbin/sendmail|; \
		s|/bin/ksh|/bin/bash|; \
		s|/usr/local/bin/vm|/usr/bin/vm|; \
	' < $i > $i.fixed && mv $i.fixed $i`;
	exit ($? >> 8) if ($? >> 8);
}
