# Vietnamese Translation for mgetty.
# Copyright © 2005 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: mgetty 1.1.33-2\n"
"Report-Msgid-Bugs-To: aba@not.so.argh.org\n"
"POT-Creation-Date: 2007-08-13 10:42+0000\n"
"PO-Revision-Date: 2005-06-10 15:07+0930\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <gnomevi-list@lists.sourceforge.net>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"

#. Type: boolean
#. Description
#: ../mgetty-fax.templates.master:1001
msgid "Run faxrunqd during system startup?"
msgstr "Chạy «faxrunqd» trong khi khởi động hệ thống không?"

#. Type: boolean
#. Description
#: ../mgetty-fax.templates.master:1001
msgid ""
"The mgetty-fax package contains a daemon (\"faxrunqd\") that can "
"automatically take in charge the sending of the faxes spooled with faxspool. "
"This daemon and the faxrunq utility need /etc/mgetty/faxrunq.config to be "
"configured appropriately in order to run. If you plan to use faxrunqd, "
"please indicate so."
msgstr ""
"Gói tin «mgetty-fax» chứa một trình nền (dæmon) được gọi là «faxrunqd» mà có "
"thể tự động quản lý việc gửi các fax do «faxspool» tập hợp. Để chạy cả hai "
"trình nền này và tiện ích «faxrunq» đều thì phải cấu hình tập tin «/etc/"
"mgetty/faxrunq.config» một cách thích hợp. Nếu bạn định sử dụng «faxrunqd», "
"hãy ngụ ý như thế tại đây."

#~ msgid "New permission handling"
#~ msgstr "Cách quản lý quyền hạn"

#~ msgid ""
#~ "Since version 1.1.29, mgetty-fax is using a new security scheme. This "
#~ "implies some changes in the permission handling of certain files and "
#~ "directories."
#~ msgstr ""
#~ "Từ phiên bản 1.1.29, trình mgetty-fax có dùng một lược đồ bảo mật mới. Vì "
#~ "vậy, cách quản lý quyền hạn đã thay đổi cho một số tập tin và thư mục đều."

#~ msgid ""
#~ "A new utility with name faxq-helper has been placed into /usr/lib/mgetty-"
#~ "fax. It is owned by user \"uucp\" and is setuid. The directory /var/spool/"
#~ "fax/outoing is now owned by uucp as well and is only accessible to that "
#~ "user by default. If you want to modify the permissions of this directory "
#~ "(for example because you trust your users enough to let them read "
#~ "outgoing faxes), please use dpkg-statoverride."
#~ msgstr ""
#~ "Một tiện ích mới tên «faxq-helper» đã được chèn vào «/usr/lib/mgetty-"
#~ "fax». Người dùng «uucp» sở hữu nó, và đã lập nó «setuid». Người dùng "
#~ "«uucp» cũng sở hữu thư mục «/var/spool/fax/outoing» thì mặc định là chỉ "
#~ "người dùng ấy có thể truy cập nó. Nếu bạn muốn sửa đổi quyền hạn cho thư "
#~ "mục này (lấy thí dụ, vì bạn muốn cho phép các người dùng đọc mọi fax đã "
#~ "gửi đi) thì hãy sử dụng «dpkg-statoverride»."
