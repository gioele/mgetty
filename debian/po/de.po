#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
msgid ""
msgstr ""
"Project-Id-Version: mgetty\n"
"Report-Msgid-Bugs-To: aba@not.so.argh.org\n"
"POT-Creation-Date: 2007-08-13 10:42+0000\n"
"PO-Revision-Date: 2004-04-19 11:16+0100\n"
"Last-Translator: Andreas Barth <aba@not.so.argh.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../mgetty-fax.templates.master:1001
msgid "Run faxrunqd during system startup?"
msgstr "faxrunqd w�hrend des Systemstarts starten?"

#. Type: boolean
#. Description
#: ../mgetty-fax.templates.master:1001
msgid ""
"The mgetty-fax package contains a daemon (\"faxrunqd\") that can "
"automatically take in charge the sending of the faxes spooled with faxspool. "
"This daemon and the faxrunq utility need /etc/mgetty/faxrunq.config to be "
"configured appropriately in order to run. If you plan to use faxrunqd, "
"please indicate so."
msgstr ""
"Das mgetty-fax Paket enth�lt ein Systemprogramm (\"faxrunqd\"), das "
"automatisch Faxe versenden kann, die mit faxspool zum Senden vorgemerkt "
"werden. Damit Faxen mit diesem oder faxrunq versendet werden k�nnen, mu� /"
"etc/mgetty/faxrunq.config angepasst werden. Bitte geben Sie an, ob faxrunqd "
"verwendet werden soll."

#~ msgid "New permission handling"
#~ msgstr "Neue Zugriffsrechtbehandlung"

#~ msgid ""
#~ "Since version 1.1.29, mgetty-fax is using a new security scheme. This "
#~ "implies some changes in the permission handling of certain files and "
#~ "directories."
#~ msgstr ""
#~ "Seit Version 1.1.29 benutzt mgetty-fax ein neues Sicherheitsschema. "
#~ "Dieses impliziert einige �nderungen bei der Behandlung der Zugriffsrechte "
#~ "von verschiedenen Dateien und Verzeichnissen."

#~ msgid ""
#~ "A new utility with name faxq-helper has been placed into /usr/lib/mgetty-"
#~ "fax. It is owned by user \"uucp\" and is setuid. The directory /var/spool/"
#~ "fax/outoing is now owned by uucp as well and is only accessible to that "
#~ "user by default. If you want to modify the permissions of this directory "
#~ "(for example because you trust your users enough to let them read "
#~ "outgoing faxes), please use dpkg-statoverride."
#~ msgstr ""
#~ "Es wurde ein neues Hilfsprogramm namens faxq-helper in /usr/lib/mgetty-"
#~ "fax installiert. Es geh�rt dem Benutzer \"uucp\" und ist setuid. Das "
#~ "Verzeichnis /var/spool/fax/outgoing geh�rt nun ebenfalls uucp und ist nur "
#~ "f�r diesen Benutzer zug�nglich. Wenn Sie die Zugriffsrechte von diesem "
#~ "Verzeichnis �ndern wollen (zum Beispiel weil Sie Ihren Benutzern genug "
#~ "trauen um sie ausgehende Faxe direkt lesen zu lassen), benutzen Sie dpkg-"
#~ "statoverride."
